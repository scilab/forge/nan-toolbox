// Copyright (C) 2010 - 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.


//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);

//
// Generate the ambiguity help
// mprintf("Updating nan/file_io\n");
// helpdir = fullfile(cwd,"file_io");
// funmat = [
//   "xptopen"
//   ];
// macrosdir = cwd +"../../macros/help_files_sci";
// //demosdir = cwd +"../../demos";
// demosdir = [];
// modulename = "nan";
// helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )


//
// Generate the ambiguity help
mprintf("Updating nan/basic\n");
helpdir = fullfile(cwd,"basic");
funmat = [
  //"nansum"
  "nan_covm"
  "nan_ecovm"
  "nan_decovm"
  "nan_xcovf"
  "nan_conv2nan"
  "nan_cor"         // cor namenskonflikt
  "nan_cov"         //cov existiert in stixbox
  "nan_corrcoef"
  "nan_rankcorr"
  "nan_partcorrcoef"
  "nan_tiedrank"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )
//
// Generate the amplitude help
mprintf("Updating nan/descriptive_statistics\n");
helpdir = fullfile(cwd,"descriptive_statistics");
funmat = [
  "nan_mean" //mean existiert in scilab
  "nan_var"                  // namenskonflikt mit var
  "nan_std"   // std existiert in stixbox
  //"nanmean"  //existiert in scilab
  //"nanstd"
  "nan_median" // median existiert in scilab
  "nan_iqr"       //iqr existiert in scilab
  "nan_trimmean"   //trimmean existiert in scilab
  "nan_center"    //center existiert in scilab
  "nan_geomean"      //geomean existiert in scilab
  "nan_detrend"      //detrend existiert in scilab
  "nan_moment"    //moment existiert in scilab
  "nan_medAbsDev"
  "nan_sem"
  "nan_meansq"
  "nan_sumsq"
  "nan_mad"
  "nan_rms"
  "nan_statistic"
  "nan_skewness"
  "nan_kurtosis"
  "nan_zscore"
  "nan_zScoreMedian"
  "nan_meanAbsDev" 
  "nan_harmmean"
  "nan_spearman"
  "nan_ranks"
  "nan_quantile"     //quantile existiert in stixbox
  "nan_prctile"
  "nan_percentile"
  "nan_trimean"
  "nan_meandev"
  "nan_ecdf"
  //"normpdf"
  //"normcdf"
  //"norminv"
  //"tpdf"
  //"tcdf"
  //"tinv"
  "nan_coef_of_variation"
  "nan_hist2res"
  "nan_histo"
  "nan_histo2"
  "nan_histo3"
  "nan_histo4"
  "nan_histc"
  "nan_grpstats"
  "nan_y2res"
  "nan_cumsum"
  "nan_filter"

  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )

//
// Generate the ambiguity help
mprintf("Updating nan/statistical_visualization\n");
helpdir = fullfile(cwd,"statistical_visualization");
funmat = [
 "nan_bland_altmann"
 "nan_cdfplot"
  "nan_gscatter"
  "nan_boxplot"
  "nan_normplot"
  "nan_andrewsplot"
  "nan_hist"
  "nan_ecdfhist"
  "nan_fscatter3"
  "nan_plotmatrix"
  "nan_gplotmatrix"
  "nan_parallelcoords"
  "nan_errorb"
  "nan_errorbar"
  "nan_nhist"
  "nan_qqplot"
  "nan_probplot"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )
//
// Generate the ambiguity help
mprintf("Updating nan/anova\n");
helpdir = fullfile(cwd,"anova");
funmat = [
  "nan_anova"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )

//
// Generate the datasets help
mprintf("Updating nan/classification\n");
helpdir = fullfile(cwd,"classification");
funmat = [
"nan_train_sc"
"nan_test_sc"
"nan_classify"
"nan_xval"
"nan_kappa"
"nan_train_lda_sparse"
"nan_fss"
"nan_cat2bin"
"nan_row_col_deletion"
"nan_mahal"
"nan_rocplot"
"nan_confusionmat"
"nan_partest"

  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )




//
// Generate the datasets help
mprintf("Updating nan/cluster_analysis\n");
helpdir = fullfile(cwd,"cluster_analysis");
funmat = [
"nan_kmeans"
  "nan_pdist"
  "nan_linkage"
  "nan_squareform"
  "nan_cluster"
  "nan_crosstab"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )


// Generate the ambiguity help
mprintf("Updating nan/other\n");
helpdir = fullfile(cwd,"other");
funmat = [
 "sumskipnan"
//  "rem"
 // "mod"
  "flag_nans_occured"
  "flag_impl_skip_nan"
  "flag_accuracy_level"
  "flag_impl_significance"
  "nan_grp2idx"
   "nan_mgrp2idx"
     "nan_getpath"
   //"nan_flix"
  //"str2array"
  ];
macrosdir = cwd +"../../macros";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )

// Generate the ambiguity help
mprintf("Updating nan/other\n");
helpdir = fullfile(cwd,"other");
funmat = [
  "str2array"
  ];
macrosdir = cwd +"../../macros/help_files_sci";
//demosdir = cwd +"../../demos";
demosdir = [];
modulename = "nan";
helptbx_helpupdate  ( funmat , helpdir , macrosdir , demosdir , modulename , %t )

