function [F,X]=nan_ecdf(h,Y,opt_plot)
    //  empirical cumulative function
    // Calling Sequence
    //  [F,X] = nan_ecdf(Y)
    //  nan_ecdf(Y)
    //  nan_ecdf(gca,Y)
    //  nan_ecdf(...,'plot')
    // Parameters
    // Y :	input data, must be a vector or matrix, in case Y is a matrix, the ecdf for every column is computed.
    //   'plot':	if one input parameter is 'plot', it plots the empirical cdf, in axis gca.
    //   [F,X]:   empirical cumulative distribution functions (i.e Kaplan-Meier estimate)
    // Description
    //  NaN's are considered Missing values and are ignored.
    //  Examples
    //  y = grand(50,1,'exp',10);
    //  [F,X]=nan_ecdf(y,'plot');
    //  xx = 0:.1:max(y);
    //  yy = 1-exp(-xx/10);
    //  plot(xx,yy,'g-','LineWidth',2)
    //  legend('Empirical','Population',4);
    //
    // See also
    // nan_histo2
    // nan_histo3
    // nan_percentile
    // nan_quantile
    // nan_ecdfhist
    // Authors
    // Copyright (C) 2009,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt
    //
    //



    //	$Id$
    //	Copyright (C) 2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    mod=ieee();
    ieee(2);
    in_par=["h","Y","opt_plot"];
    in_par_min=1;
    plotting=0;
    if (nargin>in_par_min)
        if (type(evstr(in_par(nargin)))==10)
            if convstr(evstr(in_par(nargin)))=="plot",   plotting=1; end;
            clear evstr(in_par(nargin));
            nargin=nargin-1;
        end;
    end;
    clear in_par;


    if ~sum(length(h))==1 | ~typeof(h)=="handle" | typeof(h)=="st",
        Y = h;
        h = [];
    end;

    DIM = [];

    SW = typeof(Y)=="st";
    //if SW, SW = isfield(Y,'datatype'); end;
    if SW,  allfields=getfield(1,Y);SW = or(allfields(3:$)=="datatype"); end;
    if SW, SW = (Y.datatype=="HISTOGRAM"); end;
    if SW,
        [yr,yc]=size(Y.H);
        //if ~isfield(Y,'N');
        allfields=getfield(1,Y);
        if ~or(allfields(3:$)=="N")
            Y.N = sum(Y.H,1);
        end;
        f = [zeros(1,yc);cumsum(Y.H,1)];
        for k=1:yc,
            f(:,k)=f(:,k)/Y.N(k);
        end;
        t = [Y.X(1,:);Y.X];

    elseif or(type(Y)==[1 5 8]),
        sz = size(Y);
        if isempty(DIM),
            DIM = min(find(sz>1));
            if isempty(DIM), DIM = 1; end;
        end;
        if DIM==2, Y=Y.'; DIM = 1; end;

        t = mtlb_sort(Y,1);
        t = [t(1,:);t];
        N = sum(~isnan(Y),1);
        f = zeros(size(Y,1)+1,size(Y,2));
        for k=1:size(Y,2),
            f(:,k)=[0:size(Y,1)]'/N(k);
        end;
    end;

    if (plotting),
        if  ~isempty(h), sca(h); end;
        plot2d2(t,f);
    end;
    F = f;
    if nargout>1
        X = t;
    end;
    ieee(mod);
endfunction
