//function [h,ax,BigAx,patches,pax] = plotmatrix(x,y,param1,param2)
function nan_plotmatrix(x,y,param1,param2)
    // Scatter plot matrix.
    // Calling Sequence
    // nan_plotmatrix(Y)
    // nan_plotmatrix(X,Y)
    // nan_plotmatrix(...,LineSpec)
    // nan_plotmatrix(AX,...)
    // Parameters
    // LineSpec: line specification, '.' is the default (see help plot)
    // AX: uses AX as the BigAx instead of gca()
    // Description
    //   plotmatrix(X,Y) scatter plots the columns of X against the columns
    //   of Y.  If X is P-by-M and Y is P-by-N, PLOTMATRIX will produce a
    //   N-by-M matrix of axes. plotmatrix(Y) is the same as plotmatrix(Y,Y)
    //   except that the diagonal will be replaced by hist(Y(:,i)).
    //
    //   Examples
    //       x = rand(50,3,'normal'); y = x*[-1 2 1;2 0 1;1 -2 3;]';
    //       nan_plotmatrix(y,'r*')
    //   Authors
    //   H. Nahrstaedt
    if nargin<1
        error("Requires one Parameter");
    elseif nargin <3 then
        param1=[];
        param2=[];
    elseif nargin < 4 then
        param2=[];
    end
    // Accept an axes as the first arg, and plot into it.
    if nargin>0 & sum(length(x))==1 & typeof(x)=="handle"
        // Get the axes, and shift the remaining args down by one.
        cax = x;
        x = y;
        if nargin > 2
            y = param1;
            if nargin > 3
                param1 = param2;
                param2=[];
            end;
        end
        nin = nargin - 1;
    else
        cax = gca();
        nin = nargin;
    end


    linespec = "."; // Default symbol.
    have_hist = %f;

    if type(param1)==10,
        linespec = param1;
        have_line_spec=%t;
        nin = nin - 1;
    end

    if type(y)==10,
        linespec = y;
        have_line_spec=%t;
        nin = nin - 1;
    end

    if nin==1, // plotmatrix(y)
        rows = size(x,2); cols = rows;
        x = x; y = x;
        have_hist = %t;
    elseif nin==2, // plotmatrix(x,y)
        rows = size(y,2); cols = size(x,2);
        x = x; y = y;
    else
        error("Wrong number of input variables");
    end

    if isempty(rows) | isempty(cols),
        if nargout>0, h = []; ax = []; BigAx = []; end
        return
    end

    if length(size(x))>2 | length(size(y))>2,
        error(" Dimension of X and Y must be 2.")
    end
    if size(x,1)~=size(y,1) | size(x,2)~=size(y,2),
        error("X and Y must have the same number of rows and pages.");
    end

    BigAx=cax;

    markersize = 3;

    xlim = zeros(rows, cols, 2);
    ylim = zeros(rows, cols, 2);

    ax=list();
    xsize = 0.9 / cols;
    ysize = 0.9 / rows;
    xoff = 0.05;
    yoff = 0.05;
    border = [0.130, 0.110, 0.225, 0.185] .* [xsize, ysize, xsize, ysize];
    border(3:4) = - border(3:4) - border(1:2);

    for i = rows:-1:1
        for j = cols:-1:1

            pos = [xsize * (j - 1) + xoff, ysize * ( i -1) + yoff, xsize, ysize];
            ax(rows*(i-1)+j) = newaxes();

            ax(rows*(i-1)+j).axes_bounds=[pos]';
            ax(rows*(i-1)+j).tight_limits="on";
            if (i == j & have_hist)

                [nn, xx] = nan_hist (x(:, i));
                bar (xx, nn, 1.0);
                ax(rows*(i-1)+j).auto_ticks(1)="on";
                xlim(i,j,:) = ax(rows*(i-1)+j).data_bounds(:,1);
                ylim(i,j,:) = [%inf, -%inf];
            else

                if (have_line_spec)
                    plot (x (:, i), y (:, j), linespec);
                else
                    plot (x (:, i), y (:, j), ".");
                end
                xlim(i,j,:) = ax(rows*(i-1)+j).data_bounds(:,1);
                ylim(i,j,:) = ax(rows*(i-1)+j).data_bounds(:,2);
            end
        end
    end

    xlimmin = min(xlim(:,:,1),"r"); xlimmax = max(xlim(:,:,2),"r");
    ylimmin = min(ylim(:,:,1),"c"); ylimmax = max(ylim(:,:,2),"c");

    inset = .15;
    for i=1:rows,
        for j=1:cols

            ax(rows*(i-1)+j).data_bounds(:,1)=[xlimmin(1,j) xlimmax(1,j)]';
            dx = diff(ax(rows*(i-1)+j).data_bounds(:,1))*inset;

            ax(rows*(i-1)+j).data_bounds(:,1)=[xlimmin(1,j)-dx xlimmax(1,j)+dx]';

            if (i==j & have_hist )
                //        ax(rows*(i-1)+j).data_bounds(:,2)=[0 ylimmax(i,1)]';
                //        dy = diff(ax(rows*(i-1)+j).data_bounds(:,2))*inset;
                //        ax(rows*(i-1)+j).data_bounds(:,2)=[0 ylimmax(i,1)+dy]';
            else
                ax(rows*(i-1)+j).data_bounds(:,2)=[ylimmin(i,1) ylimmax(i,1)]';
                dy = diff(ax(rows*(i-1)+j).data_bounds(:,2))*inset;
                ax(rows*(i-1)+j).data_bounds(:,2)=[ylimmin(i,1)-dy ylimmax(i,1)+dy]';
            end;
        end;
    end

    //  for i=1:rows-1,
    //   for j=2:cols
    //   ax(rows*(i-1)+j).x_ticks.labels=[''];
    //   ax(rows*(i-1)+j).y_ticks.labels=[''];
    //    ax(rows*(i-1)+j).margins=[0.01 0.01 0.01 0.01];
    // end
    //
    // end
    // for j=2:cols
    //  ax(rows*(rows-1)+j).y_ticks.labels=[''];
    //  ax(rows*(rows-1)+j).margins=[0.01 0.01 0.01 0.01];
    // end;
    //  for i=1:rows-1,
    //  ax(rows*(i-1)+1).x_ticks.labels=[''];
    // ax(rows*(i-1)+1).margins=[0.01 0.01 0.01 0.01];
    // end;
    // ax(rows*(rows-1)+1).margins=[0.01 0.01 0.01 0.01];
    //
    // if nargout~=0,
    //     h = ax;
    // end
endfunction