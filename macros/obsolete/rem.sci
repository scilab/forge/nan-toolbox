function [z,e] = rem(x,y)
//  calculates remainder of X / Y  
//  Calling Sequence
//    [z,e] = rem(X,Y)
// Parameters
//	z: is the remainder of X / Y
//	e: is the error tolerance, for checking the accuracy z(e > abs(y)) is not defined 
// 	z: has always the same sign than x	
// Description
//      z = x - y * fix(x/y);
//     e = eps * fix(x/y);
// Authors
// H. Nahrstaedt - Aug 2010
//	Copyright (C) 2004,2009,2010 by Alois Schloegl a.schloegl@ieee.org	

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.

//       $Id$
//	Copyright (C) 2004,2009,2010 by Alois Schloegl <a.schloegl@ieee.org>	
//       This function is part of the NaN-toolbox
//       http://biosig-consulting.com/matlab/NaN/

s = warning('query');
warning('off');

if ((length(x)~=1) & (length(y)~=1) & or(size(x)~=size(y))) 
        error('size of input arguments do not fit.');
end;

t = fix(x./y);
z = x - y.*t;

if length(x)==1,
	z(~t) = x;		// remainder is x if y = inf
else
	z(~t) = x(~t);		// remainder is x if y = inf
end;

z(~mtlb_repmat(y,size(z)./size(y))) = 0;	// remainder must be 0 if y==0

warning(s);			// reset warning status

if nargout > 1,
        e = (abs(t)*%eps);	// error interval 
        //z(e > abs(y)) = NaN;	// uncertainty of rounding error to large
end;

endfunction

