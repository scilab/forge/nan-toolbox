function y = nan_conv (a, b)
// Convolve two vectors.
// Calling Sequence
// y = nan_conv (a,b)
// y = nan_conv (a, b) returns a vector of length equal to length (a) + length (b) - 1
// Description
// If a and b are polynomial coefficient vectors, conv
// returns the coefficients of the product polynomial.
// Examples
// u = [1   2   3   4]
// v = [10   20   30]
// c = nan_conv(u,v)
// Authors
// Tony Richardson arichard@stark.cc.oh.us
// H. Nahrstaedt

// See also 
// deconv
// Author: Tony Richardson <arichard@stark.cc.oh.us>
// Created: June 1994
// Adapted-By: jwe

// Copyright (C) 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2002, 2004,
//               2005, 2006, 2007, 2008 John W. Eaton
//
// This file is part of Octave.
//
// Octave is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// Octave is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Octave; see the file COPYING.  If not, see
// <http://www.gnu.org/licenses/>.



  if (nargin ~= 2)
    error ("2 arguments needed");
  end

  if (~ (min(size(a))==1 & min(size(b))==1))
    error("nan_conv: both arguments must be vectors");
  end

  la = length (a);
  lb = length (b);

  ly = la + lb - 1;

  // Use the shortest vector as the coefficent vector to filter.
  // Preserve the row/column orientation of the longer input.
  if (la < lb)
    if (ly > lb)
      if (size (b, 1) <= size (b, 2))
        x = [b, (zeros (1, ly - lb))];
      else
        x = [b; (zeros (ly - lb, 1))];
      end
    else
      x = b;
    end
    y = filter (a, 1, x);
  else
    if(ly > la)
      if (size (a, 1) <= size (a, 2))
        x = [a, (zeros (1, ly - la))];
      else
        x = [a; (zeros (ly - la, 1))];
      end
    else
      x = a;
    end
    y = filter (b, 1, x);
  end

endfunction
