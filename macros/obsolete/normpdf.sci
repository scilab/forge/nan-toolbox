function p = normpdf(x,m,s)
// returns normal probability density 
// Calling Sequence
// pdf = normpdf(x,m,s);
// Parameters
// Computes the PDF of a the normal distribution 
//    with mean m and standard deviation s
//    default: m=0; s=1;
//    
// x,m,s must be matrices of same size, or any one can be a scalar. 
//
// See also
// normcdf
// norminv
// Authors
// Copyright (C) 2000-2003,2010 by Alois Schloegl a.schloegl@ieee.org
//  H. Nahrstaedt - 2010


//    $Id$
//    Copyright (C) 2000-2003,2010 by Alois Schloegl <a.schloegl@ieee.org>	
//    This function is part of the NaN-toolbox
//    http://biosig-consulting.com/matlab/NaN/

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; If not, see <http://www.gnu.org/licenses/>.
if nargin==1,
        m=0;s=1;
elseif nargin==2,
        s=1;
end;        

// allocate output memory and check size of argument
//z=mtlb_ones(size(x))*%inf;
//z(s~=0) = (x(s~=0)-m(s~=0))./s(s~=0);	
//z(s==0 & (x==m))=%nan;

z = (x-m)./s; // if this line causes an error, input arguments do not fit. 

//p = ((2*pi)^(-1/2))*exp(-z.^2/2)./s;
SQ2PI = 2.5066282746310005024157652848110;
p = exp(-z.^2/2)./(s*SQ2PI); 

// p=ones(size(x,1),size(x,2))*%inf;
// p(s~=0) = exp(-z(s~=0).^2/2)./(s(s~=0)*SQ2PI);
// p(s==0 & (x==m))=%nan;

p((x==m) & (s==0)) = %inf;

p(isinf(z)) = 0;

p(isnan(x) | isnan(m) | isnan(s) | (s<0)) = %nan;

endfunction