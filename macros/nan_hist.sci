function [no,xo] = nan_hist(y,x,param1,opt_plot)
    //   Produce histogram counts or plots
    //   Calling Sequence
    //   nn = nan_hist(Y)
    //   nn = nan_hist(Y,nbins)
    //   nn = nan_hist(Y,X)
    //   nan_hist(...,'plot')  produces a histogram bar plot of   the results.
    //   nan_hist(AX,...) plots into AX instead of GCA.
    //   [nn,xx] = nan_hist(...)
    //   Parameters
    //   Y: input vector or matrix. If Y is a   matrix, hist works down the columns.
    //   nbins: number of equally spaced containers in which Y is sorted [default: 10]
    //   X: X is a vector which specifies  centers of each container. The first bin includes  data between -inf and the first center and the last bin   includes data between the last bin and inf. Note: Use HISTC if  it is more natural to specify bin edges instead.
    //   nn: number of elements in each container
    //   xx: position of the bin centers in X.
    //   Description
    //   With one vector input argument, Y, produces a histogram of the valueswith 10 bins.  The range of the histogram bins is determined by the
    // range of the data.  With one matrix input argument, Y, plot a
    // histogram where each bin contains a bar per input column.
    //
    // Given a second vector argument, X, use that as the centers of
    // the bins, with the width of the bins determined from the adjacent
    // values in the vector.
    //
    // If scalar, the second argument, nbins, defines the number of bins.
    //
    //
    // Extreme values are lumped in the first and last bins.
    //
    // With two output arguments, produce the values nn and xx such
    // that bar (xx, nn) will plot the histogram.
    //
    //
    //  Examples
    //  x = -4:0.1:4;
    //  y = rand(10000,1,'normal');
    //  nan_hist(y,x,'plot')
    //   See also
    //    nan_histc
    // Authors
    // H. Nahrstaedt
    if (nargin == 0),
        error("At least 1 parameter required");
    end;

    in_par=["y","x","param1","opt_plot"];
    in_par_min=1;
    plotting=0;
    if (nargin>in_par_min)
        if (type(evstr(in_par(nargin)))==10)
            if convstr(evstr(in_par(nargin)))=="plot",   plotting=1; end;
            clear evstr(in_par(nargin));
            nargin=nargin-1;
        end;
    end;
    clear in_par;

    if nargin>0 & sum(length(y))==1 & typeof(y)=="handle"
        cax = y;
        y = x;
        if nargin > 2
            x = param1;
        end
        nargs = nargin - 1;
    else
        cax = [];
        nargs = nargin;
    end

    if nargs == 1
        x = 10;
    end


    if isvector (y) then
        y = y(:);
    end

    if (~or(type(x)==[1 5 8]) | ~or(type(y)==[1 5 8]))
        error( "Input arguments must be numeric.")
    end

    N = x;

    if isempty(y),
        if length(x) == 1,
            x = 1:double(x);
        end
        nn = zeros(x);

    else
        // Avoid issues with integer types for x and y
        x = double (x);
        y = double (y);
        ind = ~isnan(y);
        min_val = min(y(ind));
        max_val = max(y(ind));

        if (isempty(min_val))
            min_val = %nan;
            max_val = %nan;
        end

        if length(x) == 1
            if min_val == max_val,
                min_val = min_val - floor(x/2) - 0.5;
                max_val = max_val + ceil(x/2) - 0.5;
            end
            binwidth = (max_val - min_val) ./ x;
            xx = min_val + binwidth*(0:x);
            xx(length(xx)) = max_val;
            x = xx(1:length(xx)-1) + binwidth/2;
        else
            xx = x(:)';
            binwidth = [diff(xx) 0];
            xx = [xx(1)-binwidth(1)/2 xx+binwidth/2];
            xx(1) = min(xx(1),min_val);
            xx($) = max(xx($),max_val);
        end

        xx = full(real(xx));
        y = full(real(y));

        nn = nan_histc(y,[-%inf xx + %eps],1);

        nn(2,:) = nn(2,:)+nn(1,:);
        nn($-1,:) = nn($-1,:)+nn($,:);
        nn = nn(2:$-1,:);

    end


    if isvector(y) & ~isempty(y)
        no = nn';
        xo = x;
    else
        no = nn;
        xo = x';
    end
    if plotting
        if isempty(cax) then
            cax=gca();
        end;
        if (size (nn, 2) ~= 1)
            bar(cax,x,nn,0.8);
        else
            bar(cax,x,nn,1.0);
        end;
        cax.auto_ticks(1)="on";

    end

endfunction
