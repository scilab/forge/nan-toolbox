function [kap,se,H,z,p0,SA,R]=nan_kappa(d,c,arg3,w)
    //  estimates Cohen's kappa coefficient
    //   and related statistics
    // Calling Sequence
    // [...] = nan_kappa(d1,d2);
    //	NaN's are handled as missing values and are ignored
    // [...] = nan_kappa(d1,d2,'notIgnoreNAN');
    //	NaN's are handled as just another Label.
    // [kap,sd,H,z,ACC,sACC,MI] = kappa(...);
    // X = nan_kappa(...);
    // Parameters
    // d1 :   data of scorer 1
    // d2 :   data of scorer 2
    //
    // kap:	Cohen's kappa coefficient point
    // se:	standard error of the kappa estimate
    // H:	Confusion matrix
    // z:	z-score
    // ACC:	overall agreement (accuracy)
    // sACC:	specific accuracy
    // MI :	Mutual information or transfer information (in [bits])
    // X :	is a struct containing all the fields above
    // X.TP: number of true positive instances
    // X.FN: false negative instances
    // X.FP: false positive instances
    // X.TN: true negative instances
    // X.RPP : Rate of positive predictions. RPP=(TP+FP)/(TP+FN+FP+TN)
    // X.RNP : Rate of negative predictions. RNP=(TN+FN)/(TP+FN+FP+TN)
    // X.TPR : True positive rate . TPR = TP/(TP+FN)
    // X.FNR : False negative rate. FNR=FN/(TP+FN)
    // X.FPR : False positive rate. FPR=FP/(TN+FP)
    // X.TNR : True negative rate. TNR=TN/(TN+FP)
    // X.PPV : Positive predictive value. PPV=TP/(TP+FP)
    // X.NPV : Negative predictive value. NPV=TN/(TN+FN)
    //
    // Description
    // For two classes, a number of additional summary statistics including
    //  TPR, FNR, FPR, TNR, PPV, NPV,RPP, F1, dprime, Matthews Correlation coefficient (MCC), Specificity and Sensitivity
    // are provided. Note, the positive category must the larger label (in d and c), otherwise
    // the confusion matrix becomes transposed and the summary statistics are messed up.
    // Examples
    // ratings1=[0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
    // ratings2=[0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
    // X = nan_kappa(ratings1,ratings2)
    //
    //
    // Bibliography
    // [1] Cohen, J. (1960). A coefficient of agreement for nominal scales. Educational and Psychological Measurement, 20, 37-46.
    // [2] J Bortz, GA Lienert (1998) Kurzgefasste Statistik f|r die klassische Forschung, Springer Berlin - Heidelberg.
    //        Kapitel 6: Uebereinstimmungsmasze fuer subjektive Merkmalsurteile. p. 265-270.
    // [3] http://www.cmis.csiro.au/Fiona.Evans/personal/msc/html/chapter3.html
    // [4] Kraemer, H. C. (1982). Kappa coefficient. In S. Kotz and N. L. Johnson (Eds.),
    //        Encyclopedia of Statistical Sciences. New York: John Wiley & Sons.
    // [5] http://ourworld.compuserve.com/homepages/jsuebersax/kappa.htm
    // [6] http://en.wikipedia.org/wiki/Receiver_operating_characteristic
    // Authors
    // Copyright (c) 1997-2006,2008,2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (c) 1997-2006,2008,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/
    //
    //    BioSig is free software: you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation, either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    BioSig is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with BioSig.  If not, see <http://www.gnu.org/licenses/>.
    mod=ieee();
    ieee(2);
    kmode.ignoreNAN = 1;
    kk = [];
    if nargin>2
        if type(arg3)==10
            if (arg3=="notIgnoreNAN")
                kmode.ignoreNAN = 0;
            end
        else
            kk = arg3;
        end
    end;
    if nargin<4
        w = [];
    end;

    if nargin>1,
        d = d(:);
        c = c(:);

        tmp = [d;c];
        maxtmp = max(tmp);
        tmp(isnan(tmp)) = maxtmp+1;

        [X.Label,i,j]   = moc_unique(tmp);
        c = j(1+length(d):$);
        d = j(1:length(d));
        kk = max(j);
        maxCLASS = kk - or(tmp>maxtmp);

        if kmode.ignoreNAN,
            if or(j > maxCLASS)
                //			fprintf(2,'Warning KAPPA: some elements are NaN. These are handled as missing values and are ignored.\n');
                //			fprintf(2,'If NaN should be handled as just another label, use kappa(..,''notIgnoreNaN'').\n');
                ix = find((c<=maxCLASS) & (d<=maxCLASS));
                d = d(ix); c=c(ix);
                if ~isempty(w), w = w(ix); end;
                kk = kk - 1;
            end;
            X.Label(X.Label>maxtmp) = [];
        else
            X.Label(X.Label>maxtmp) = %nan;
        end;

        if isempty(w) then
            H = full( sparse ([d, c], ones(d), [kk, kk]) );
        elseif ~isempty(w),
            H = full( sparse ([d, c], w, [kk, kk]) );
        end;

    else
        X.Label = 1:min(size(d));
        H = d(X.Label,X.Label);
    end;
    s = warning("query");
    warning("off");

    N = sum(H(:));
    p0  = sum(diag(H))/N;  //accuracy of observed agreement, overall agreement
    //OA = sum(diag(H))/N);

    p_i = sum(H,1);
    pi_ = sum(H,2)';

    SA  = 2*diag(H)'./(p_i+pi_); // specific agreement

    pe  = (p_i*pi_')/(N*N);  // estimate of change agreement

    px  = sum(p_i.*pi_.*(p_i+pi_))/(N*N*N);

    //standard error
    kap = (p0-pe)/(1-pe);
    sd  = sqrt((pe+pe*pe-px)/(N*(1-pe*pe)));

    //standard error
    se  = sqrt((p0+pe*pe-px)/N)/(1-pe);
    if ~isreal(se),
        z = %nan;
    else
        z = kap/se;
    end


    if ((1 < nargout) & (nargout<7))
        warning(s);
        ieee(mod);
        return;
    end;

    // Nykopp's entropy
    pwi = sum(H,2)/N;                       // p(x_i)
    pwj = sum(H,1)/N;                       // p(y_j)
    pji = H./repmat(sum(H,2),1,size(H,2));  // p(y_j | x_i)
    R   = - sumskipnan(pwj.*log2(pwj)) + sumskipnan(pwi'*(pji.*log2(pji)));

    if (nargout>1),ieee(mod); return; end;

    X.kappa = kap;
    X.kappa_se = se;
    X.data = H;
    X.H = X.data;
    X.z = z;
    X.ACC = p0;
    X.sACC = SA;
    X.MI = R;
    X.datatype="confusion";

    if size(H,1)==2 & size(H,2)==2,
        // see http://en.wikipedia.org/wiki/Receiver_operating_characteristic
        // Note that the confusion matrix used here is has positive values in
        // the 2nd row and column, moreover the true values are indicated by
        // rows (transposed). Thus, in summary H(1,1) and H(2,2) are exchanged
        // as compared to the wikipedia article.
        X.TP  = H(1,1);
        X.TN  = H(2,2);
        X.FP  = H(1,2);
        X.FN  = H(2,1);
        X.FNR = X.FN/(X.TP+X.FN);
        X.FPR = X.FP/(X.TN+X.FP);
        X.TPR = X.TP/(X.TP+X.FN);
        X.TNR=X.TN/(X.TN+X.FP);

        X.PPV = X.TP/(X.TP+X.FP);
        X.NPV = X.TN/(X.TN+X.FN);

        X.RNP=(X.TN+X.FN)/(X.TP+X.FN+X.FP+X.TN);
        X.RPP=(X.TP+X.FP)/(X.TP+X.FN+X.FP+X.TN);

        X.FDR = X.FP / (X.TP+X.FP);

        X.MCC = det(H) / sqrt(prod([mtlb_sum(H), mtlb_sum(H')]));
        //X.F1  = 2 * X.TP / (sum(H(2,:)) + sum(H(:,2)));
        X.F1 = 2*X.TP / (2*X.TP + X.FP + X.FN);
        X.Sensitivity = X.TPR;	// hit rate, recall
        X.Specificity = 1 - X.FPR;
        X.Precision   = X.PPV;
        if X.TPR>0 & X.TPR<1 & X.FDR>0 & X.FDR<1 then
            X.dprime = cdfnor("X",0,1,X.TPR,1-X.TPR) - cdfnor("X",0,1,X.FDR,1-X.FDR);
        end;

        //         X.TNR = H(1,1)/sum(H(1,:));

    end;
    ieee(mod);
    kap = X;
    warning(s);
endfunction



