function nan_normplot(x)
    // Produce a normal probability plot for each column of X.
    // Calling Sequence
    // normplot(x)
    // Description
    // The line joing the 1st and 3rd quantile is drawn on the
    // graph.  If the underlying distribution is normal, the
    // points will cluster around this line.
    //
    // Note that this function sets the title, xlabel, ylabel,
    // axis, grid, tics and hold properties of the graph.  These
    // need to be cleared before subsequent graphs using 'clf'.
    // Examples
    // Y=grand(1,25,'nor',0,1);
    // nan_normplot(Y);
    // Authors
    // H. Nahrstaedt
    // Paul Kienzle pkienzle@users.sf.net
    if size(x,1)==1
        x = x';
    end
    [n, m] = size(x);

    [sx,originds] = mtlb_sort(x);

    minx  = min(sx(:));
    maxx  = max(sx(:));
    if isnan(minx) // Data all %nans, setting arbitrary limits.
        minx = 0;
        maxx = 1;
    end
    xrange = maxx-minx;

    if xrange>0
        minxaxis  = minx-0.025*xrange;
        maxxaxis  = maxx+0.025*xrange;
    else
        minxaxis  = minx - 1;
        maxxaxis  = maxx + 1;
    end

    // Use the same Y vector if all columns have the same count.
    if (~or(isnan(x(:))))
        eprob = ((1:n)' - 0.5)./n;
    else
        nvec = sum(~isnan(x));
        eprob = repmat((1:n)', 1, m);
        eprob = (eprob-.5) ./ repmat(nvec, n, 1);
        eprob(isnan(sx)) = %nan;
    end


    y  = cdfnor("X",zeros(eprob),ones(eprob),eprob,1-eprob);

    minyaxis  = cdfnor("X",0,1,0.25 ./n,1- (0.25 ./n));
    maxyaxis  = cdfnor("X",0,1,(n-0.25) ./n,1- ((n-0.25) ./n));

    p     = [0.001 0.003 0.01 0.02 0.05 0.10 0.25 0.5...
    0.75 0.90 0.95 0.98 0.99 0.997 0.999];

    label = ["0.001";"0.003"; "0.01";"0.02";"0.05";"0.10";"0.25";"0.50"; ...
    "0.75";"0.90";"0.95";"0.98";"0.99";"0.997"; "0.999"];

    tick  = cdfnor("X",zeros(p),ones(p),p,1-p);

    q1x = nan_prctile(x,25);
    q3x = nan_prctile(x,75);
    q1y = nan_prctile(y,25);
    q3y = nan_prctile(y,75);
    qx = [q1x; q3x];
    qy = [q1y; q3y];


    dx = q3x - q1x;
    dy = q3y - q1y;
    slope = dy./dx;
    centerx = (q1x + q3x)/2;
    centery = (q1y + q3y)/2;
    maxx = max(x);
    minx = min(x);
    maxy = centery + slope.*(maxx - centerx);
    miny = centery - slope.*(centerx - minx);
    yinter = centery - slope.*(centerx);

    mx = [minx; maxx];
    my = [miny; maxy];


    // Plot data and corresponding reference lines in the same color,
    // following the default color order.  Plot reference line first,
    // followed by the data, so that data will be on top of reference line.
    scf();
    plot(mx,my,"r-.");
    plot(qx,qy,"r-");
    plot(sx,y,"b+");

    // if nargout>0
    //     h = sce();
    // end

    a=gca();
    y_ticks = tlist(["ticks","locations","labels"], [], []);
    y_ticks.labels=label(tick>minyaxis & tick<maxyaxis);
    y_ticks.locations=tick(tick>minyaxis & tick<maxyaxis)';
    a.y_ticks=y_ticks;
    a.data_bounds=[minxaxis maxxaxis,minyaxis maxyaxis];
    a.tight_limits="on";
    xlabel("Data");
    ylabel("Probability");
    title("Normal Probability Plot");
    xgrid(1);

endfunction
