function [o,v]=nan_std(x,opt,DIM,W)
    // calculates the standard deviation.
    //  Calling Sequence
    // [y,v] = nan_std(x [, opt[, DIM [, W]]])
    // Parameters
    // opt :  option
    //	0:  normalizes with N-1 [default]
    //		provides the square root of best unbiased estimator of the variance
    //	1:  normalizes with N,
    //		this provides the square root of the second moment around the mean
    // 	otherwise:
    //               best unbiased estimator of the standard deviation (see [1])
    //
    // DIM:	dimension
    // 	N STD of  N-th dimension
    //	default or []: first DIMENSION, with more than 1 element
    // W:	weights to compute weighted s.d. (default: [])
    //	if W=[], all weights are 1.
    //	number of elements in W must match size(x,DIM)
    //
    // y:	estimated standard deviation
    // Description
    // features:
    //
    // - provides an unbiased estimation of the S.D.
    // - can deal with NaN's (missing values)
    // - weighting of data
    // - dimension argument also in Octave
    // - compatible to Matlab and Octave
    //
    // Examples
    // X=testmatrix('magi',3)
    // X([1 6:9]) = %nan*ones(1,5)
    // X  =
    //    Nan   1.    Nan
    //    3.    5.    Nan
    //    4.    Nan   Nan
    //  y = nan_std(X)
    //  y =
    //    0.7071068    2.8284271    Nan
    // See also
    //nan_rms
    //sumskipnan
    //nan_mean
    //nan_var
    //nan_meansq
    //
    // Bibliography
    // [1] http://mathworld.wolfram.com/StandardDeviationDistribution.html
    // Authors
    //	Copyright (C) 2000-2003,2006,2009,2010 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.

    //	$Id$
    //	Copyright (C) 2000-2003,2006,2009,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //       This is part of the NaN-toolbox for Octave and Matlab
    //       http://biosig-consulting.com/matlab/NaN/
    if nargin<4,
        W = [];
    end;
    if nargin<3,
        DIM = [];
    end;
    if isempty(DIM),
        DIM = find(size(x)>1,1);
        if isempty(DIM), DIM=1; end;
    end;

    mod=ieee();
    ieee(2);

    [y,n,ssq] = sumskipnan(x,DIM,W);
    if and(ssq(:).*n(:) > 2*(y(:).^2))
        //// rounding error is neglectable
        y = ssq - y.*y./n;
    else
        //// rounding error is not neglectable
        szx = size(x);
        szy = size(y);
        if length(szy)<length(szx);
            szy(length(szy)+1:length(szx)) = 1;
        end;
        [y,n] = sumskipnan((x-repmat(y./n,szx./szy)).^2,DIM,W);
    end;


    if nargin<2,
        opt = 0;
    end;
    if isempty(opt),
        opt = 0;
    end;


    if opt==0,
        // square root if the best unbiased estimator of the variance
        ib = %inf;
        o  = sqrt(y./max(n-1,0));	// normalize
    elseif opt==1,
        ib = %nan;
        o  = sqrt(y./n);

    else
        // best unbiased estimator of the mean
        if exists("unique"),
            // usually only a few n's differ
            //[N,tmp,tix] = unique(n(:));	// compress n and calculate ib(n)
            [N] = unique(n(:));
            ib = sqrt(N/2).*gamma((N-1)./2)./gamma(N./2);	//inverse b(n) [1]
            [sY ,idx] =  gsort(n(:),"g","i");
            [tmp,idx] =  gsort(idx,"g","i");         // generate inverse index
            ix  = diff(sY,1)>0;
            tix = cumsum([1;ix]);	// rank
            [yr,yc]=size(n);
            tix = matrix(tix(idx),yr,yc);
            ib = ib(matrix(tix,size(y)));	// expand ib to correct size

        elseif exists("histo3"),
            // usually only a few n's differ
            [N,tix] = histo3(n(:)); N = N.X;
            ib = sqrt(N/2).*gamma((N-1)./2)./gamma(N./2);	//inverse b(n) [1]
            ib = ib(matrix(tix,size(y)));	// expand ib to correct size

        else	// gamma is called prod(size(n)) times
            ib = sqrt(n/2).*gamma((n-1)./2)./gamma(n./2);	//inverse b(n) [1]
        end;
        ib = matrix(ib,size(y));
        o  = sqrt(y./n).*ib;
    end;

    if nargout>1,
        v = y.*((max(n-1,0)./(n.*n))-1 ./(n.*ib.*ib)); // variance of the estimated S.D. ??? needs further checks
    end;

endfunction

