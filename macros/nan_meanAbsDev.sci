function R = nan_meanAbsDev(i,DIM)
    // estimates the Mean Absolute deviation
    // Calling Sequence
    // y = nan_meanAbsDev(x,DIM)
    // Parameters
    //  y: the mean deviation of x in dimension DIM
    // DIM	dimension
    //	1: STATS of columns
    //	2: STATS of rows
    //	default or []: first DIMENSION, with more than 1 element
    // Description
    // estimates the Mean Absolute deviation
    // (note that according to [1,2] this is the mean deviation;
    // not the mean absolute deviation)
    //
    // features:
    // - can deal with NaN's (missing values)
    // - dimension argument
    // - compatible to Matlab and Octave
    //Examples
    //
    // salery=[950; 1200; 1370; 1580; 1650; 1800; 6000; %nan];
    // nan_meanAbsDev(salery);
    //
    //
    // See also
    // sumskipnan
    // nan_medAbsDev
    // nan_var
    // nan_std
    //
    // Bibliography
    // [1] http://mathworld.wolfram.com/MeanDeviation.html
    // [2] L. Sachs, "Applied Statistics: A Handbook of Techniques", Springer-Verlag, 1984, page 253.
    // [3] http://mathworld.wolfram.com/MeanAbsoluteDeviation.html
    // [4] Kenney, J. F. and Keeping, E. S. "Mean Absolute Deviation." §6.4 in Mathematics of Statistics, Pt. 1, 3rd ed. Princeton, NJ: Van Nostrand, pp. 76-77 1962.
    //Authors
    //	Copyright (C) 2000-2002,2010 by Alois Schloegl a.schloegl@ieee.org
    //	H. Nahrstaedt - 2010

    //	$Id$
    //	Copyright (C) 2000-2002,2010 by Alois Schloegl <a.schloegl@ieee.org>
    //    	This is part of the NaN-toolbox. For more details see
    //       http://biosig-consulting.com/matlab/NaN/

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 2 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; If not, see <http://www.gnu.org/licenses/>.
    if nargin==1,
        DIM = find(size(i)>1,1);
        if isempty(DIM), DIM=1; end;
    end;

    mod=ieee();
    ieee(2);
    [S,N] = sumskipnan(i,DIM);		// sum
    i     = i - repmat(S./N,size(i)./size(S));		// remove mean
    [S,N] = sumskipnan(abs(i),DIM);		//

    //if flag_implicit_unbiased_estim;    //// ------- unbiased estimates -----------
    if 0,
        n1 	= max(N-1,0);			// in case of n=0 and n=1, the (biased) variance, STD and STE are INF
    else
        n1	= N;
    end;
    R     = S./n1;
    ieee(mod);
endfunction
