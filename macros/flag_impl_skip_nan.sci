function FLAG = flag_impl_skip_nan(FLAG)
    //  sets and gets default mode for handling NaNs
    //  Calling Sequence
    //   FLAG = flag_impl_skip_nan()
    //   flag_impl_skip_nan(FLAG)
    //  prevFLAG = flag_impl_skip_nan(nextFLAG)
    //   Parameters
    //   FLAG: gets current mode
    //	1 :skips NaN's (the default mode if no mode is set)
    // 	0: NaNs are propagated; input NaN's give NaN's at the output
    // Description
    // prevFLAG = flag_impl_skip_nan(nextFLAG)
    //	gets previous set FLAG and sets FLAG for the future
    //
    // flag_impl_skip_nan(prevFLAG)
    //	resets FLAG to previous mode
    //
    // It is used in:
    //	sumskipnan, median, quantiles, trimean
    // and affects many other functions like:
    //	center, kurtosis, mad, mean, moment, rms, sem, skewness,
    //	statistic, std, var, zscore etc.
    //
    // The mode is stored in the global variable FLAG_impl_skip_nan
    // It is recommended to use flag_impl_skip_nan(1) as default and
    // flag_impl_skip_nan(0) should be used for exceptional cases only.
    // This feature might disappear without further notice, so you should really not
    // rely on it.
    // Authors
    // 	Copyright (C) 2001-2003,2009 by Alois Schloegl a.schloegl@ieee.org
    // H. Nahrstaedt - 2010

    //    This program is free software; you can redistribute it and/or modify
    //    it under the terms of the GNU General Public License as published by
    //    the Free Software Foundation; either version 3 of the License, or
    //    (at your option) any later version.
    //
    //    This program is distributed in the hope that it will be useful,
    //    but WITHOUT ANY WARRANTY; without even the implied warranty of
    //    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    //    GNU General Public License for more details.
    //
    //    You should have received a copy of the GNU General Public License
    //    along with this program; if not, write to the Free Software
    //    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    //	$Id$
    // 	Copyright (C) 2001-2003,2009 by Alois Schloegl <a.schloegl@ieee.org>
    //       This function is part of the NaN-toolbox
    //       http://biosig-consulting.com/matlab/NaN/

    global FLAG_impl_skip_nan;

    //// if strcmp(version,'3.6'), FLAG_impl_skip_nan=(1==1); end;	//// hack for the use with Freemat3.6

    ////// set DEFAULT value of FLAG
    if isempty(FLAG_impl_skip_nan) then
        FLAG_impl_skip_nan = %t; //logical(1); // logical.m not available on 2.0.16
    end;

    FLAG = FLAG_impl_skip_nan;
    if nargin > 0 then
        FLAG_impl_skip_nan = (FLAG~=0); //logical(i); //logical.m not available in 2.0.16
        if (~FLAG)
            warning("flag_impl_skipnan(0): You are warned!!! You have turned off skipping NaN in sumskipnan. This is not recommended. Make sure you really know what you do.")
        end;
    end;
endfunction
