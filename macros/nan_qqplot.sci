function nan_qqplot(x,y,pvec)
    // Produce a empirical quantile-quantile plot plot
    // Calling Sequence
    // nan_qqplot(x)
    // nan_qqplot(x,y)
    // nan_qqplot(x,y,pvec)
    // Parameters
    // pvec: specify the quantiles for plotting in a vector
    // Description
    // nan_qqplot(x) display a quantile-quantile plot of x versus a normal distribution. If the distribution of x is normal, then the plot is linear.
    //
    // nan_qqplot(x,y) display a quantile-quantile plot of x versus a quantile-quantile plot of y. If the distribution of x and y is the same, then the plot is almost linear.
    //
    // Examples
    // x = grand(100,1,"nor",0,1);
    // y =grand(100,1,"bet",5,100);
    // nan_qqplot(x,y);
    // Authors
    // H. Nahrstaedt
    //


    if nargin == 1

        if size(x,1)==1
            x = x';
        end
        [y,origindy]  =  mtlb_sort(x);


        [n, m] = size(y);
        if n == 1
            y = y';
            n = m;
            m = 1;
        end

        nvec = sum(~isnan(y));
        x = repmat((1:n)', 1, m);
        x = (x-.5) ./ repmat(nvec, n, 1);
        x(isnan(y)) = %nan;



        origindx = [];
        //x = norminv(x);
        x = distfun_norminv(x);
        //x = cdfnor("X",zeros(x),ones(x),x,1-x)

        xx = x;
        yy = y;
        xlab = "Standard Normal Quantiles";
        ylab = "Quantiles of Input Sample";
        tlab = "QQ Plot of Sample Data versus Standard Normal";
    else

        if size(x,1)==1
            x = x';
        end
        if size(y,1)==1
            y = y';
        end
        n = -1;
        xlab = "X Quantiles";
        ylab = "Y Quantiles";
        tlab = "";

        if nargin < 3

            nx = sum(~isnan(x));
            if (length(nx) > 1)
                nx = max(nx);
            end
            ny = sum(~isnan(y));
            if (length(ny) > 1)
                ny = max(ny);
            end
            n = min(nx, ny);
            pvec = 100*((1:n) - 0.5) ./ n;
        end
        if size(x,1)==n
            xx = zeros(x);
            origindx = zeros(x);
            nancols = find(or(isnan(x),1));
            fullcols = find(and(~isnan(x),1));
            [xx(:,fullcols),origindx(:,fullcols)] = mtlb_sort(x(:,fullcols));
            xx(:,nancols) = nan_prctile(x(:,nancols),pvec);
            if size(x,2)==1 & size(y,2)~=1
                origindx = repmat(origindx,1,size(y,2));
            end

        else
            xx = nan_prctile(x,pvec);
            origindx = [];
        end
        if size(y,1)==n
            yy = zeros(y);
            origindy = zeros(y);
            nancols = find(or(isnan(y),1));
            fullcols = find(and(~isnan(y),1));
            [yy(:,fullcols),origindy(:,fullcols)] = mtlb_sort(y(:,fullcols));
            yy(:,nancols) = nan_prctile(y(:,nancols),pvec);
            if size(y,2)==1 & size(x,2)~=1
                origindy = repmat(origindy,1,size(x,2));
            end
        else
            yy = nan_prctile(y,pvec);
            origindy = [];
        end
    end




    q1x = nan_prctile(x,25);
    q3x = nan_prctile(x,75);
    q1y = nan_prctile(y,25);
    q3y = nan_prctile(y,75);
    qx = [q1x; q3x];
    qy = [q1y; q3y];


    dx = q3x - q1x;
    dy = q3y - q1y;
    slope = dy./dx;
    centerx = (q1x + q3x)/2;
    centery = (q1y + q3y)/2;
    maxx = mtlb_max(x);
    minx = mtlb_min(x);
    maxy = centery + slope.*(maxx - centerx);
    miny = centery - slope.*(centerx - minx);

    mx = [minx; maxx];
    my = [miny; maxy];

    scf();
    plot(mx,my,"r-.");
    plot(qx,qy,"r-");
    plot(xx,yy,"b+");



    a=gca();

    a.tight_limits="on";

    xlabel(xlab);
    ylabel(ylab);
    title (tlab);
    // xgrid(1);


endfunction