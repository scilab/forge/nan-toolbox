function y = nan_squareform (x, method)
    // Convert a vector from the nan_pdist function into a square matrix or from a square matrix back to the vector form.
    // Calling Sequence
    // y = nan_squareform (x)
    // y = nan_squareform (x, "tovector")
    // y = nan_squareform (x,"tomatrix")
    // Description
    //
    // Convert a vector from the pdist function into a square matrix or from
    // a square matrix back to the vector form.
    //
    // The second argument is used to specify the output type in case there
    // is a single element
    // Examples
    // Y=(1:6)'
    // X=nan_squareform(Y)
    // X  =
    //
    //    0.    1.    2.    3.
    //    1.    0.    4.    5.
    //    2.    4.    0.    6.
    //    3.    5.    6.    0.
    // nan_squareform(X)
    // ans  =
    //
    //    1.
    //    2.
    //    3.
    //    4.
    //    5.
    //    6.
    // See also
    // nan_pdist
    // Authors
    // H. Nahrstaedt - 2010
    //  Bill Denney bill@denney.ws

    // Copyright (C) 2006, 2008  Bill Denney  <bill@denney.ws>
    //
    // This is free software; you can redistribute it and/or modify it under
    // the terms of the GNU General Public License as published by the Free
    // Software Foundation; either version 2, or (at your option) any later
    // version.
    //
    // This software is distributed in the hope that it will be useful, but
    // WITHOUT ANY WARRANTY; without even the implied warranty of
    // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    // General Public License for more details.
    //
    // You should have received a copy of the GNU General Public License
    // along with this software; see the file COPYING.  If not, see
    // <http://www.gnu.org/licenses/>.

    if nargin < 1
        error("y = nan_squareform (x)");
    elseif nargin < 2
        if length(x)==1 | (size(x,1)==1 | size(x,2)==1) then
            method = "tomatrix";
        elseif size(x,1)==size(x,2) then
            method = "tovector";
        else
            error ("squareform: cannot deal with a nonsquare, nonvector   input");
        end
    end
    method = convstr (method);

    if ~or(["tovector" "tomatrix"] == method)
        error ("squareform: method must be either tovector or  tomatrix\n");
    end
    mod=ieee();
    ieee(2);

    if ("tovector" == method) then
        if ~ (size(x,1)==size(x,2)) then
            ieee(mod);
            error ("squareform: x is not a square matrix");

        end

        sx = size (x, 1);
        y = zeros ((sx-1)*sx/2, 1);
        idx = 1;
        for i = 2:sx
            newidx = idx + sx - i;
            y(idx:newidx) = x(i:sx,i-1);
            idx = newidx + 1;
        end
    else
        // we're converting to a matrix

        // make sure that x is a column
        x = x(:);

        // the dimensions of y are the solution to the quadratic formula
        // for:
        // length(x) = (sy-1)*(sy/2)
        sy = (1 + sqrt (1+ 8*max(size (x))))/2;
        y = zeros (sy,sy);
        for i = 1:sy-1
            step = sy - i;
            y((sy-step+1):sy,i) = x(1:step);
            x(1:step) = [];
        end
        y = y + y';
    end
    ieee(mod);
endfunction

