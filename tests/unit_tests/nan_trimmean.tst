// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
computed = nan_trimmean([11.4, 17.3, 21.3, 25.9, 40.1],.2);
expected = 23.2;
assert_checkalmostequal ( computed , expected , %eps );

