// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
[nn,xx]=nan_hist([1:4],3);
assert_checkequal(xx, [1.5,2.5,3.5]);
assert_checkequal(nn, [1,1,2]);

[nn,xx]=nan_hist([1:4]',3);
assert_checkequal(xx, [1.5,2.5,3.5]);
assert_checkequal(nn, [1,1,2]);

[nn,xx]=nan_hist([1 1 1 %nan %nan %nan 2 2 3],[1 2 3]);
assert_checkequal(xx, [1,2,3]);
assert_checkequal(nn, [3,2,1]);

[nn,xx]=nan_hist([[1:4]',[1:4]'],3);
assert_checkequal(xx, [1.5;2.5;3.5]);
assert_checkequal(nn, [[1,1,2]',[1,1,2]']);

assert_checkequal(nan_hist(1,1),1);

for n = [10, 30, 100, 1000]
    assert_checkequal(sum(nan_hist([1:n], n)), n);
    assert_checkequal(sum(nan_hist([1:n], [2:n-1])), n);
    assert_checkequal(sum(nan_hist([1:n], [1:n])), n);
    assert_checkequal(sum(nan_hist([1:n], 29)), n);
    assert_checkequal(sum(nan_hist([1:n], 30)), n);
end
// %!test
assert_checkequal (size (nan_hist(rand(750,240,"normal"), 200)), [200,240]);