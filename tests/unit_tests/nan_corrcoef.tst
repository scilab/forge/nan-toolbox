// <-- NO CHECK REF -->
// Copyright (C) 2010 - H. Nahrstaedt
//
x = rand (10,10);
cc1 = nan_corrcoef (x);
cc2 = nan_corrcoef (x, x);
assert_checktrue((size (cc1) == [10, 10] & size (cc2) == [10, 10] & and(abs (cc1 - cc2) < sqrt (%eps))));
//assert_checkalmostequal(nan_corrcoef (5), 1,%eps);

// %% Test input validation
// error corrcoef ();
// error corrcoef (1, 2, 3);
// error corrcoef ([true, true]);
// error corrcoef ([1, 2], [true, true]);
// error corrcoef (ones (2,2,2));
// error corrcoef (ones (2,2), ones (2,2,2));
