// Copyright (C) 2010 - H. Nahrstaedt
//
pct = 50;
q = nan_prctile (1:4, pct, 1);
qa = [1, 2, 3, 4];
assert_checkequal (q, qa);

q = nan_prctile (1:4, pct, 2);
qa = 2.5000;
assert_checkequal (q, qa);

pct = 50;
x = [0.1126, 0.1148, 0.0521, 0.2364, 0.1393
    0.1718, 0.7273, 0.2041, 0.4531, 0.1585
    0.2795, 0.7978, 0.3296, 0.5567, 0.7307
    0.4288, 0.8753, 0.6477, 0.6287, 0.8165
    0.9331, 0.9312, 0.9635, 0.7796, 0.8461];
tol = 0.0001;
q = nan_prctile (x, pct, 1);
qa = [0.2795, 0.7978, 0.3296, 0.5567, 0.7307];
assert_checkalmostequal (q, qa, tol);
q = nan_prctile (x, pct, 2);
qa = [0.1148; 0.2041; 0.5567; 0.6477; 0.9312];
assert_checkalmostequal (q, qa, tol);

pct = 50;
tol = 0.0001;
x = [0.1126, 0.1148, 0.0521, 0.2364, 0.1393
    0.1718, 0.7273, 0.2041, 0.4531, 0.1585
    0.2795, 0.7978, 0.3296, 0.5567, 0.7307
    0.4288, 0.8753, 0.6477, 0.6287, 0.8165
    0.9331, 0.9312, 0.9635, 0.7796, 0.8461];
x(5,5) = %inf;
q = nan_prctile (x, pct, 1);
qa = [0.2795, 0.7978, 0.3296, 0.5567, 0.7307];
assert_checkalmostequal (q, qa, tol);

x(5,5) = -%inf;
q = nan_prctile (x, pct, 1);
qa = [0.2795, 0.7978, 0.3296, 0.5567, 0.1585];
assert_checkalmostequal (q, qa, tol);

x(1,1) = %inf;
q = nan_prctile (x, pct, 1);
qa = [0.4288, 0.7978, 0.3296, 0.5567, 0.1585];
assert_checkalmostequal (q, qa, tol);

pct = 50;
tol = 0.0001;
x = [0.1126, 0.1148, 0.0521, 0.2364, 0.1393
    0.1718, 0.7273, 0.2041, 0.4531, 0.1585
    0.2795, 0.7978, 0.3296, 0.5567, 0.7307
    0.4288, 0.8753, 0.6477, 0.6287, 0.8165
    0.9331, 0.9312, 0.9635, 0.7796, 0.8461];
x(3,3) = %inf;
q = nan_prctile (x, pct, 1);
qa = [0.2795, 0.7978, 0.6477, 0.5567, 0.7307];
assert_checkalmostequal (q, qa, tol);

q = nan_prctile (x, pct, 2);
qa = [0.1148; 0.2041; 0.7307; 0.6477; 0.9312];
assert_checkalmostequal (q, qa, tol);

pct = 50;
tol = 0.001;
x = [0.1126, 0.1148, 0.0521, 0.2364, 0.1393
    0.1718, 0.7273, 0.2041, 0.4531, 0.1585
    0.2795, 0.7978, 0.3296, 0.5567, 0.7307
    0.4288, 0.8753, 0.6477, 0.6287, 0.8165
    0.9331, 0.9312, 0.9635, 0.7796, 0.8461];
x(5,5) = %nan;
q = nan_prctile (x, pct, 2);
qa = [0.1148; 0.2041; 0.5567; 0.6477; 0.9322];
assert_checkalmostequal (q, qa, tol);

x(1,1) = %nan;
q = nan_prctile (x, pct, 2);
qa = [0.1270; 0.2041; 0.5567; 0.6477; 0.9322];
assert_checkalmostequal (q, qa, tol);
x(3,3) = %nan;
q = nan_prctile (x, pct, 2);
qa = [0.1270; 0.2041; 0.6437; 0.6477; 0.9322];
assert_checkalmostequal (q, qa, tol);

// Test input validation
// error nan_prctile ()
// error nan_prctile (1, 2, 3, 4)
// error nan_prctile ([true, false], 10)
// error nan_prctile (1:10, [true, false])
// error nan_prctile (1, 1, 1.5)
// error nan_prctile (1, 1, 0)
// error nan_prctile (1, 1, 3)

