// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()

CURRENT_PATH = strsubst(get_absolute_file_path("builder_gateway_c.sce"), "\", "/");

lib_name = "nan_c";


table = ["covm_mex", "covm_mex","cmex"; ..
          "kth_element", "kth_element","cmex"; ..
          //"xptopen", "xptopen","cmex"; ..
          "sumskipnan_mex", "sumskipnan_mex","cmex"; ..
          "histo_mex", "histo_mex","cmex"; ..
          "str2array", "str2array","cmex"; ..
          "knnclass_mex", "knnclass_mex","cmex"; ..
];

files = ["covm_mex.cpp", ..
          "kth_element.cpp", ..
          "histo_mex.cpp", ..
          "sumskipnan_mex.cpp", ..
          "str2array.c", ..
          "knnclass_mex.c"];//,"xptopen.cpp"];



libs = [""];
cc = "";
fflags = "";

if getos()<>"Windows" then
    if (getos() == "Darwin") then
        cflags = "-D__SCILAB__""";
        ldflags= "";
    else
        ldflags = "-lstdc++ "
        cflags = " -D__SCILAB__"
    end
else
    ldflags = "";
    cflags = "-D__SCILAB__";
end

//ilib_mex_build(lib_name, table, files, libs, [], ldflags, cflags,fflags);
tbx_build_gateway(lib_name, table, files, CURRENT_PATH, [], ldflags, cflags, fflags, cc, [], %t);

endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack


