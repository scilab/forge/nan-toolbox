mode(-1)
// Compares performance  



x=rand(1000000,1);

tic(),timer(); M1=median(thrownan(x));RES=[timer() toc()];
printf(' median(thownan())  needs \t%3.3f cputime, %3.3fs for %i samples.\n',RES,length(x));


tic();timer(); M_nan1=nan_median(nan_median(x));RES=[timer() toc()];
printf('nan_median       needs \t%3.3f cputime, %3.3fs for %i samples .\n',RES,length(x));
    


tic(),timer(); M2=mean(thrownan(x));RES=[timer() toc()];
printf(' mean(thownan()) needs \t%3.3f cputime, %3.3fs for %i samples.\n',RES,length(x));

tic();timer(); M_nan2=nan_mean(nan_mean(x));RES=[timer() toc()];
printf('nan_mean        needs \t%3.3f cputime, %3.3fs for %i samples .\n',RES,length(x));
    


tic(),timer(); M3=sum(thrownan(x));RES=[timer() toc()];
printf(' sum(thownan()) needs \t%3.3f cputime, %3.3fs for %i samples.\n',RES,length(x));

tic();timer(); M_nan3=sumskipnan(x);RES=[timer() toc()];
printf('sumskipnan       needs \t%3.3f cputime, %3.3fs for %i samples .\n',RES,length(x));
    
